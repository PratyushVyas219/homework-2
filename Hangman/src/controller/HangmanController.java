package controller;

import apptemplate.AppTemplate;
import com.sun.corba.se.impl.orbutil.graph.Graph;
import data.GameData;
import gui.Workspace;
import javafx.animation.AnimationTimer;
import javafx.application.Platform;
import javafx.geometry.Insets;
import javafx.scene.Group;
import javafx.scene.Scene;
import javafx.scene.canvas.Canvas;
import javafx.scene.canvas.GraphicsContext;
import javafx.scene.control.Alert;
import javafx.scene.control.Button;
import javafx.scene.control.Label;
import javafx.scene.input.KeyEvent;
import javafx.scene.layout.BorderPane;
import javafx.scene.layout.HBox;
import javafx.scene.layout.StackPane;
import javafx.scene.paint.Color;
import javafx.scene.paint.Paint;
import javafx.scene.shape.Rectangle;
import javafx.scene.text.Text;
import javafx.stage.FileChooser;
import javafx.stage.FileChooser.ExtensionFilter;
import propertymanager.PropertyManager;
import ui.AppMessageDialogSingleton;
import ui.YesNoCancelDialogSingleton;

import java.io.File;
import java.io.IOException;
import java.nio.file.Path;
import java.nio.file.Paths;
import java.util.Random;

import static settings.AppPropertyType.*;
import static settings.InitializationParameters.APP_WORKDIR_PATH;

/**
 * @author Ritwik Banerjee
 */
public class HangmanController implements FileController {

    public enum GameState {
        UNINITIALIZED,
        INITIALIZED_UNMODIFIED,
        INITIALIZED_MODIFIED,
        ENDED
    }
    private Canvas canvas;
    private GraphicsContext graphic;
    private AppTemplate appTemplate; // shared reference to the application
    private GameData    gamedata;    // shared reference to the game being played, loaded or saved
    private GameState   gamestate;   // the state of the game being shown in the workspace
    private Text[]      progress;    // reference to the text area for the word
    private boolean     success;     // whether or not player was successful
    private int         discovered;  // the number of letters already discovered
    private Button      gameButton;  // shared reference to the "start game" button
    private Button      hintButton;

    private HBox        guesses;
    private Label       remains;     // dynamically updated label that indicates the number of remaining guesses
    private Path        workFile;

    public HangmanController(AppTemplate appTemplate, Button gameButton) {
        this(appTemplate);
        this.gameButton = gameButton;
    }

    public HangmanController(AppTemplate appTemplate) {
        this.appTemplate = appTemplate;
        this.gamestate = GameState.UNINITIALIZED;
    }

    public void enableGameButton() {
        if (gameButton == null) {
            Workspace workspace = (Workspace) appTemplate.getWorkspaceComponent();
            gameButton = workspace.getStartGame();
            hintButton= workspace.getHintGame();
        }
        gameButton.setDisable(false);
    }

    public void disableGameButton() {
        if (gameButton == null) {
            Workspace workspace = (Workspace) appTemplate.getWorkspaceComponent();
            gameButton = workspace.getStartGame();
            hintButton= workspace.getHintGame();
        }
        gameButton.setDisable(true);
    }

    public void setGameState(GameState gamestate) {
        this.gamestate = gamestate;
    }

    public GameState getGamestate() {
        return this.gamestate;
    }

    /**
     * In the homework code given to you, we had the line
     * gamedata = new GameData(appTemplate, true);
     * This meant that the 'gamedata' variable had access to the app, but the data component of the app was still
     * the empty game data! What we need is to change this so that our 'gamedata' refers to the data component of
     * the app, instead of being a new object of type GameData. There are several ways of doing this. One of which
     * is to write (and use) the GameData#init() method.
     */
    public void start() {
        hintButton.setVisible(true);
        gamedata = (GameData) appTemplate.getDataComponent();
        success = false;
        discovered = 0;

        Workspace gameWorkspace = (Workspace) appTemplate.getWorkspaceComponent();




        gamedata.init();
        setGameState(GameState.INITIALIZED_UNMODIFIED);
        HBox remainingGuessBox = gameWorkspace.getRemainingGuessBox();
        HBox guessedLetters    = (HBox) gameWorkspace.getGameTextsPane().getChildren().get(1);
        remains = new Label(Integer.toString(GameData.TOTAL_NUMBER_OF_GUESSES_ALLOWED));
        remainingGuessBox.getChildren().addAll(new Label("Remaining Guesses: "), remains);
        initWordGraphics(guessedLetters);


        play();
    }

    public void hangmanbody(int remaininguess, BorderPane figurepane){
        Canvas canvas = new Canvas();
        canvas.setStyle("-fx-background-color: cyan");
        GraphicsContext graphic = canvas.getGraphicsContext2D();
        canvas.setWidth(figurepane.getWidth());
        canvas.setHeight(figurepane.getHeight());
        figurepane.getChildren().addAll(canvas);
        if(remaininguess<=9) {
            graphic.setFill(Paint.valueOf("#000000"));
            graphic.fillRect(20, 350, 300, 10);
        }
        if(remaininguess<=8) {
            graphic.setFill(Paint.valueOf("#000000"));
            graphic.fillRect(20, 0, 10, 350);
        }
        if(remaininguess<=7) {
            graphic.setFill(Paint.valueOf("#000000"));
            graphic.fillRect(20, 0, 150, 10);
        }
        if(remaininguess<=6) {
            graphic.setFill(Paint.valueOf("#000000"));
            graphic.fillRect(170, 0, 10, 50);
        }
        if(remaininguess<=5) {
            graphic.setFill(Paint.valueOf("#000000"));
            graphic.fillOval(125, 40, 100, 100);
        }
        if(remaininguess<=4) {
            graphic.beginPath();
            graphic.setStroke(Paint.valueOf("#000000"));
            graphic.setLineWidth(10);
            graphic.strokeLine(170, 140, 120, 200);
        }
        if(remaininguess<=3) {
            graphic.beginPath();
            graphic.setStroke(Paint.valueOf("#000000"));
            graphic.setLineWidth(10);
            graphic.strokeLine(180, 140, 230, 200);
        }
        if(remaininguess<=2) {
            graphic.setFill(Paint.valueOf("#000000"));
            graphic.fillRect(170, 140, 10, 100);
        }
        if(remaininguess<=1) {
            graphic.beginPath();
            graphic.setStroke(Paint.valueOf("#000000"));
            graphic.setLineWidth(10);
            graphic.strokeLine(170, 240, 120, 300);
        }
        if(remaininguess==0) {
            graphic.beginPath();
            graphic.setStroke(Paint.valueOf("#000000"));
            graphic.setLineWidth(10);
            graphic.strokeLine(180, 240, 230, 300);
        }

    }


    public void getHint(){
            boolean successs=true;
            Random rand = new Random();
            int randomNum = rand.nextInt(progress.length);
            while(progress[randomNum].isVisible()==true){
                randomNum = rand.nextInt(progress.length);
            }
            progress[randomNum].setVisible(true);
            gamedata.setRemainingGuesses(gamedata.getRemainingGuesses()-1);
            remains.setText(Integer.toString(gamedata.getRemainingGuesses()));
            gamedata.setHintused(true);
            hintButton.setDisable(true);
            for(int i=0;i<progress.length;i++){
                if(progress[i].isVisible()!=true){
                    successs=false;
                }
        }
        if(successs)
            success=true;
    }


    private void end() {
        Workspace gameWorkspace = (Workspace) appTemplate.getWorkspaceComponent();
        HBox guessedLetters    = (HBox) gameWorkspace.getGameTextsPane().getChildren().get(1);
        guessedLetters.getChildren().clear();
        for (int i = 0; i < progress.length; i++) {
            StackPane letter = new StackPane();
            Rectangle rect = new Rectangle(20, 20);
            StackPane.setMargin(rect, new Insets(0, 2, 0, 2));
            if(progress[i].isVisible()==true){
                rect.setFill(Color.YELLOW);
            }
            else if (progress[i].isVisible() == false) {
                rect.setFill(Color.RED);

            }
            progress[i].setVisible(true);
            letter.getChildren().addAll(rect,progress[i]);
            guessedLetters.getChildren().addAll(letter);
            }

        hintButton.setVisible(false);
        appTemplate.getGUI().getPrimaryScene().setOnKeyTyped(null);
        gameButton.setDisable(true);
        setGameState(GameState.ENDED);
        appTemplate.getGUI().updateWorkspaceToolbar(gamestate.equals(GameState.INITIALIZED_MODIFIED));
        Platform.runLater(() -> {
            PropertyManager           manager    = PropertyManager.getManager();
            AppMessageDialogSingleton dialog     = AppMessageDialogSingleton.getSingleton();
            String                    endMessage = manager.getPropertyValue(success ? GAME_WON_MESSAGE : GAME_LOST_MESSAGE);
            //if (!success)
               // endMessage += String.format(" (the word was \"%s\")", gamedata.getTargetWord());
            if (dialog.isShowing())
                dialog.toFront();
            else
                dialog.show(manager.getPropertyValue(GAME_OVER_TITLE), endMessage);
        });
    }


    private void initWordGraphics(HBox guessedLetters) {
        char[] targetword = gamedata.getTargetWord().toCharArray();
        progress = new Text[targetword.length];
        for (int i = 0; i < progress.length; i++) {
            //
            StackPane letter = new StackPane();
            Rectangle rect = new Rectangle(20,20);
            StackPane.setMargin(rect,new Insets(0,2,0,2));
            rect.setFill(Color.YELLOW);
            progress[i] = new Text(Character.toString(targetword[i]));
            progress[i].setVisible(false);
            letter.getChildren().addAll(rect,progress[i]);

            guessedLetters.getChildren().addAll(letter);
        }

    }

    public void guesses(char guess){

        Workspace gameWorkspace = (Workspace) appTemplate.getWorkspaceComponent();
        HBox guesses = gameWorkspace.getGuesses();
        boolean correct = false;
        char[] targetword = gamedata.getTargetWord().toCharArray();
        String word = new String(targetword);
        for (int i = 0; i < targetword.length; i++) {
            if (guess == word.charAt(i)) {
                correct = true;
            }
        }
        Text inn = new Text(Character.toString(guess));
        StackPane charr = new StackPane();
        Rectangle rect = new Rectangle(20,20);
        StackPane.setMargin(rect,new Insets(0,2,0,2));
        if(correct)
            rect.setFill(Color.GREEN);
        else
            rect.setFill(Color.GRAY);

        charr.getChildren().addAll(rect,inn);
        guesses.getChildren().addAll(charr);
    }

    public void play() {
        Workspace gameWorkspace = (Workspace) appTemplate.getWorkspaceComponent();
        BorderPane figurepane=gameWorkspace.getFigurePane();
        hangmanbody(gamedata.getRemainingGuesses(),figurepane);
        char[] targetword = gamedata.getTargetWord().toCharArray();
        String word = new String(targetword);
        int uniqueletters= word.length();
        for (int i = 0; i < targetword.length; i++) {
            if (i != word.indexOf(targetword[i])) {
                uniqueletters--;
            }
        }
        if(uniqueletters<=7) {
            hintButton.setDisable(true);
        }
        else{
            hintButton.setDisable(false);
        }
        hintButton.setVisible(true);

        disableGameButton();
        AnimationTimer timer = new AnimationTimer() {
            @Override
            public void handle(long now) {
                appTemplate.getGUI().updateWorkspaceToolbar(gamestate.equals(GameState.INITIALIZED_MODIFIED));
                appTemplate.getGUI().getPrimaryScene().setOnKeyTyped((KeyEvent event) -> {
                    hangmanbody(gamedata.getRemainingGuesses(),figurepane);
                    char guess = event.getCharacter().charAt(0);
                    guess = Character.toLowerCase(guess);
                    if(!(Character.isLetter(guess))){}

                    else if (!alreadyGuessed(guess)) {
                        boolean goodguess = false;
                        for (int i = 0; i < progress.length; i++) {
                            if (gamedata.getTargetWord().charAt(i) == guess) {
                                progress[i].setVisible(true);
                                gamedata.addGoodGuess(guess);
                                goodguess = true;
                                discovered++;
                            }
                        }
                        if (!goodguess) {
                            gamedata.addBadGuess(guess);
                            hangmanbody(gamedata.getRemainingGuesses(),figurepane);
                        }
                        guesses(guess);
                        success = (discovered == progress.length);
                        remains.setText(Integer.toString(gamedata.getRemainingGuesses()));
                    }
                    setGameState(GameState.INITIALIZED_MODIFIED);
                });
                if (gamedata.getRemainingGuesses() <= 0 || success){
                    stop();}
            }

            @Override
            public void stop() {
                hintButton.setVisible(false);
                super.stop();
                end();
            }
        };
        timer.start();
    }

    private void restoreGUI() {

        if(gamedata.isHintused()==true){
            hintButton.setDisable(true);
        }

        disableGameButton();
        Workspace gameWorkspace = (Workspace) appTemplate.getWorkspaceComponent();
        gameWorkspace.reinitialize();

        HBox guessedLetters = (HBox) gameWorkspace.getGameTextsPane().getChildren().get(1);
        HBox guesses = (HBox) gameWorkspace.getGameTextsPane().getChildren().get(2);
        restoreWordGraphics(guessedLetters,guesses);

        BorderPane figurePane =gameWorkspace.getFigurePane();

        figurePane.setPrefHeight(400);
        figurePane.setPrefWidth(400);


        HBox remainingGuessBox = gameWorkspace.getRemainingGuessBox();
        remains = new Label(Integer.toString(gamedata.getRemainingGuesses()));
        remainingGuessBox.getChildren().addAll(new Label("Remaining Guesses: "), remains);

        hangmanbody(gamedata.getRemainingGuesses(),figurePane);
        success = false;

        play();
    }

    private void restoreWordGraphics(HBox guessedLetters,HBox guesses) {
        discovered = 0;
        char[] targetword = gamedata.getTargetWord().toCharArray();
        progress = new Text[targetword.length];

        for (int i = 0; i < progress.length; i++) {
            StackPane letter = new StackPane();
            Rectangle rect = new Rectangle(20,20);
            StackPane.setMargin(rect,new Insets(0,2,0,2));
            progress[i] = new Text(Character.toString(targetword[i]));
            rect.setFill(Color.YELLOW);
            progress[i].setVisible(gamedata.getGoodGuesses().contains(progress[i].getText().charAt(0)));
            letter.getChildren().addAll(rect,progress[i]);
            if (progress[i].isVisible())
                discovered++;
            guessedLetters.getChildren().addAll(letter);
        }

        Object[] badguesse = gamedata.getBadGuesses().toArray();
        Text sample;
        for(int i =0;i<gamedata.getBadGuesses().size();i++){
            StackPane letterr = new StackPane();
            Rectangle rectt = new Rectangle(20,20);
            StackPane.setMargin(rectt,new Insets(0,2,0,2));
            rectt.setFill(Color.GRAY);
            sample = new Text(Character.toString((char)badguesse[i]));
            sample.setVisible(true);

            letterr.getChildren().addAll(rectt,sample);
            guesses.getChildren().addAll(letterr);
        }

        Object[] goodguesse = gamedata.getGoodGuesses().toArray();
        Text samples;
        for(int i =0;i<gamedata.getGoodGuesses().size();i++){
            StackPane letterrr = new StackPane();
            Rectangle recttt = new Rectangle(20,20);
            StackPane.setMargin(recttt,new Insets(0,2,0,2));
            recttt.setFill(Color.GREEN);
            samples = new Text(Character.toString((char)goodguesse[i]));
            samples.setVisible(true);
            letterrr.getChildren().addAll(recttt,samples);
            guesses.getChildren().addAll(letterrr);
        }

    }

    private boolean alreadyGuessed(char c) {
        return gamedata.getGoodGuesses().contains(c) || gamedata.getBadGuesses().contains(c);
    }

    @Override
    public void handleNewRequest() {

        AppMessageDialogSingleton messageDialog   = AppMessageDialogSingleton.getSingleton();
        PropertyManager           propertyManager = PropertyManager.getManager();
        boolean                   makenew         = true;
        if (gamestate.equals(GameState.INITIALIZED_MODIFIED))
            try {
                makenew = promptToSave();
            } catch (IOException e) {
                messageDialog.show(propertyManager.getPropertyValue(NEW_ERROR_TITLE), propertyManager.getPropertyValue(NEW_ERROR_MESSAGE));
            }
        if (makenew) {
            appTemplate.getDataComponent().reset();                // reset the data (should be reflected in GUI)
            appTemplate.getWorkspaceComponent().reloadWorkspace(); // load data into workspace
            ensureActivatedWorkspace();                            // ensure workspace is activated
            workFile = null;                                       // new workspace has never been saved to a file
            ((Workspace) appTemplate.getWorkspaceComponent()).reinitialize();
            enableGameButton();
        }
        if (gamestate.equals(GameState.ENDED)) {
            appTemplate.getGUI().updateWorkspaceToolbar(false);
            Workspace gameWorkspace = (Workspace) appTemplate.getWorkspaceComponent();
            gameWorkspace.reinitialize();
        }

    }

    @Override
    public void handleSaveRequest() throws IOException {
        PropertyManager propertyManager = PropertyManager.getManager();
        if (workFile == null) {
            FileChooser filechooser = new FileChooser();
            Path        appDirPath  = Paths.get(propertyManager.getPropertyValue(APP_TITLE)).toAbsolutePath();
            Path        targetPath  = appDirPath.resolve(APP_WORKDIR_PATH.getParameter());
            filechooser.setInitialDirectory(targetPath.toFile());
            filechooser.setTitle(propertyManager.getPropertyValue(SAVE_WORK_TITLE));
            String description = propertyManager.getPropertyValue(WORK_FILE_EXT_DESC);
            String extension   = propertyManager.getPropertyValue(WORK_FILE_EXT);
            ExtensionFilter extFilter = new ExtensionFilter(String.format("%s (*.%s)", description, extension),
                                                            String.format("*.%s", extension));
            filechooser.getExtensionFilters().add(extFilter);
            File selectedFile = filechooser.showSaveDialog(appTemplate.getGUI().getWindow());
            if (selectedFile != null)
                save(selectedFile.toPath());
        } else
            save(workFile);
    }

    @Override
    public void handleLoadRequest() throws IOException {
        boolean load = true;
        if (gamestate.equals(GameState.INITIALIZED_MODIFIED))
            load = promptToSave();
        if (load) {
            PropertyManager propertyManager = PropertyManager.getManager();
            FileChooser     filechooser     = new FileChooser();
            Path            appDirPath      = Paths.get(propertyManager.getPropertyValue(APP_TITLE)).toAbsolutePath();
            Path            targetPath      = appDirPath.resolve(APP_WORKDIR_PATH.getParameter());
            filechooser.setInitialDirectory(targetPath.toFile());
            filechooser.setTitle(propertyManager.getPropertyValue(LOAD_WORK_TITLE));
            String description = propertyManager.getPropertyValue(WORK_FILE_EXT_DESC);
            String extension   = propertyManager.getPropertyValue(WORK_FILE_EXT);
            ExtensionFilter extFilter = new ExtensionFilter(String.format("%s (*.%s)", description, extension),
                                                            String.format("*.%s", extension));
            filechooser.getExtensionFilters().add(extFilter);
            File selectedFile = filechooser.showOpenDialog(appTemplate.getGUI().getWindow());
            if (selectedFile != null && selectedFile.exists())
                load(selectedFile.toPath());

            restoreGUI(); // restores the GUI to reflect the state in which the loaded game was last saved
        }
    }

    @Override
    public void handleExitRequest() {
        try {
            boolean exit = true;
            if (gamestate.equals(GameState.INITIALIZED_MODIFIED))
                exit = promptToSave();
            if (exit)
                System.exit(0);
        } catch (IOException ioe) {
            AppMessageDialogSingleton dialog = AppMessageDialogSingleton.getSingleton();
            PropertyManager           props  = PropertyManager.getManager();
            dialog.show(props.getPropertyValue(SAVE_ERROR_TITLE), props.getPropertyValue(SAVE_ERROR_MESSAGE));
        }
    }

    private void ensureActivatedWorkspace() {
        appTemplate.getWorkspaceComponent().activateWorkspace(appTemplate.getGUI().getAppPane());
    }

    private boolean promptToSave() throws IOException {
        PropertyManager            propertyManager   = PropertyManager.getManager();
        YesNoCancelDialogSingleton yesNoCancelDialog = YesNoCancelDialogSingleton.getSingleton();

        yesNoCancelDialog.show(propertyManager.getPropertyValue(SAVE_UNSAVED_WORK_TITLE),
                               propertyManager.getPropertyValue(SAVE_UNSAVED_WORK_MESSAGE));

        if (yesNoCancelDialog.getSelection().equals(YesNoCancelDialogSingleton.YES))
            handleSaveRequest();

        return !yesNoCancelDialog.getSelection().equals(YesNoCancelDialogSingleton.CANCEL);
    }

    /**
     * A helper method to save work. It saves the work, marks the current work file as saved, notifies the user, and
     * updates the appropriate controls in the user interface
     *
     * @param target The file to which the work will be saved.
     * @throws IOException
     */
    private void save(Path target) throws IOException {
        appTemplate.getFileComponent().saveData(appTemplate.getDataComponent(), target);
        workFile = target;
        setGameState(GameState.INITIALIZED_UNMODIFIED);
        AppMessageDialogSingleton dialog = AppMessageDialogSingleton.getSingleton();
        PropertyManager           props  = PropertyManager.getManager();
        dialog.show(props.getPropertyValue(SAVE_COMPLETED_TITLE), props.getPropertyValue(SAVE_COMPLETED_MESSAGE));
    }

    /**
     * A helper method to load saved game data. It loads the game data, notified the user, and then updates the GUI to
     * reflect the correct state of the game.
     *
     * @param source The source data file from which the game is loaded.
     * @throws IOException
     */
    private void load(Path source) throws IOException {
        // load game data
        appTemplate.getFileComponent().loadData(appTemplate.getDataComponent(), source);

        // set the work file as the file from which the game was loaded
        workFile = source;

        // notify the user that load was successful
        AppMessageDialogSingleton dialog = AppMessageDialogSingleton.getSingleton();
        PropertyManager           props  = PropertyManager.getManager();
        dialog.show(props.getPropertyValue(LOAD_COMPLETED_TITLE), props.getPropertyValue(LOAD_COMPLETED_MESSAGE));

        setGameState(GameState.INITIALIZED_UNMODIFIED);
        Workspace gameworkspace = (Workspace) appTemplate.getWorkspaceComponent();
        ensureActivatedWorkspace();
        gameworkspace.reinitialize();
        gamedata = (GameData) appTemplate.getDataComponent();
    }
}
